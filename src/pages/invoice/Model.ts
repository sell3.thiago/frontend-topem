export interface OrderSingle{
    product: string,
    observation: string,
    quantity: string,
}
export interface Order {
    order: Array<OrderSingle>
}