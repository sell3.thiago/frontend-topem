export interface Auth {
    name: string,
    email: string
}

export interface User {
    user: Array<Auth>,
    access_token: string
}
