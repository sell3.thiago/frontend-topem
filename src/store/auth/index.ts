import { ActionTree, GetterTree, Module, MutationTree } from "vuex"
import { RootState } from "@/store/types"
import { User } from "./User"
import axios from "axios"

export enum UserMutations {
    SET_USER = "SET_USER",
    SET_TOKEN = "SET_TOKEN",
}

const state :User = {
    user: [],
    access_token: ''
}

const getters: GetterTree<User, RootState> = {
    getToken(state) {
        return state.access_token
    },
    getUser(state){
        return state.user
    },
    authenticated(state) {
        if(state.user.length == 0) { return false}
        return state.access_token !== ''
    }
}
const mutations: MutationTree<User> = {
    [UserMutations.SET_USER] (state, payload) {
        state.user = [payload]
    },
    [UserMutations.SET_TOKEN] (state, payload) {
        state.access_token = payload
    }
}

const actions: ActionTree<User, RootState> = {
    async signIn ({ dispatch }, credentials) {
        const response = await axios.post('login', credentials)
        if(response.data.access_token) {
            return dispatch('attempt', response.data)
        }
    },
    async attempt({ commit, state } , data ) {
        if(data) {
            commit(UserMutations.SET_TOKEN, data.access_token)
        }
        
        if(!state.access_token) {
            return
        }
        try {
            const response  = await axios.post('profile');
            console.log(response);
            
            commit(UserMutations.SET_USER, response.data)
        } catch(e) {
            
            commit('SET_USER', null)
            commit('SET_TOKEN', null)

        }
    },
    refreshToken({ commit }, data) {
        commit(UserMutations.SET_TOKEN, data.token)
        commit(UserMutations.SET_USER, data)
    },
    async signOut({ dispatch }, credentials) {
        const response = await axios.post('logout', credentials)
        if (response.data.status == 200) {
            return dispatch('logout', response.data)
        }

    },
    async logout({ commit }) {
        commit(UserMutations.SET_TOKEN,'')
        commit(UserMutations.SET_USER,{
            user: [],
            token: ''
        })
    },
    setUser({commit}, data) {
        commit(UserMutations.SET_USER, data)
    }
}

export const UserInfo: Module<User, RootState> = {
    state,
    getters,
    mutations,
    actions
}
