import store from './index'
import axios from 'axios'
store.subscribe( (mutation) => {
    switch(mutation.type) {
        case 'SET_TOKEN':
            if(mutation.payload) {
                axios.defaults.headers.common['Authorization'] = `Bearer ${mutation.payload}`
                sessionStorage.setItem('token', mutation.payload)
            } else {
                sessionStorage.removeItem('token')
            }
            break;
        case 'SET_USER':
            if(mutation.payload) {
                sessionStorage.setItem('data', JSON.stringify(mutation.payload))
            }
            break;
    }
})
