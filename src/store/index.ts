import Vue from 'vue'
import Vuex from 'vuex'
import { UserInfo } from  './auth/index'
Vue.use(Vuex)

export default new Vuex.Store({
  mutations: {
  },
  actions: {
  },
  modules: {
    UserInfo
  }
})
