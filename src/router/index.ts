import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import store from '@/store/index'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'login',
    component: () => import('@/pages/auth/LoginComponent.vue'),
    beforeEnter: (to, from, next) => {
      if (store.getters['authenticated']) {
        return next({ name: 'home' })
      } else {
        next()
      }
    }
  },
  {
    path: '/home',
    name: 'home',
    component: () => import('@/pages/home/HomeComponent.vue'),
    beforeEnter: (to, from, next) => {
      if (!store.getters['authenticated']) {
        return next({ name: 'login' })
      } else {
        next()
      }
    }
  },
  {
    path: '/invoice',
    name: 'invoice',
    component: () => import('@/pages/invoice/InvoiceComponent.vue'),
    beforeEnter: (to, from, next) => {
      if (!store.getters['authenticated']) {
        return next({ name: 'login' })
      } else {
        next()
      }
    }
  }
]

const router = new VueRouter({
  // mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
